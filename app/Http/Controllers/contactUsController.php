<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Model\contactUsDetails;

class contactUsController extends Controller
{
    public function index(Request $request){
        $data = contactUsDetails::find('1');
        return view('admin.pages.contact-us', compact('data'));
    }
    public function contactUsPost(Request $request){

        $data = contactUsDetails::find('1');
        $data->address = json_encode($request->address);
        $data->phone_numbers = json_encode($request->phone_number);
        $data->emails = json_encode($request->email);
        $data->map_url = $request->map_url;
        $data->save();
        return redirect()->route('main.contact-us')->with(["msg"=>["success", "Contact updated Successfully"]]);
    }
}
