<?php

namespace App\Http\Controllers;

use DataTables;
use Validator;
use App\Model\Doctors;
use Illuminate\Http\Request;
use App\Model\Master\FileController;
use Illuminate\Support\Facades\Crypt;

class doctorsController extends Controller
{
    public function index(Request $request) {
        $doctors = Doctors::with("profileImage");
        if($request->ajax()){
            return DataTables::of($doctors)
                ->addIndexColumn()
                ->editColumn('image', function($row){
                    return "<img class='img-thumbnail' width='50' src=".asset($row->profileImage->c_img)." height='50'>";
                })
                ->editColumn('action', function($row){

                    $btn = '<a href="'.route("main.doctors-post",["id"=>Crypt::encrypt($row->id)]).'" class="btn btn-warning btn-xs"><i class="fa fa-edit"></></a>';
                    return $btn;

                })
                ->editColumn('status', function($row){
                    if($row->status == "Available"){

                        $status = '<label class="label label-success">Available</label>';

                    }elseif($row->status == "Unavailable"){

                        $status = '<label class="label label-danger">Unavailable</label>';

                    }
                    return $status;

                })
                ->rawColumns(['action', "image", "status"])
                ->make(true);
        }
        return view('admin.pages.doctors.index');
    }
    public function post(Request $request, $id = null) {
        if($request->isMethod("POST")){

            $rules = [
                "name"  => "required",
                "phone_number"  => "required",
                "email"  => "required",
                "status"  => "required",
            ];

            if(is_null($id)){
                $data = new Doctors;
                $rules["image"] = "required";
            }else{
                $cid = Crypt::decrypt($id);
                $data = Doctors::find($cid);
            }

            $validator = Validator::make($request->all(), $rules);

            if($validator->fails()){
                return back()
                ->withInput()
                ->withErrors($validator);
            }

            $data->name = $request->name;
            $data->phone_number = $request->phone_number;
            $data->email = $request->email;
            $data->status = $request->status;
            if($data->save()){
                if($request->file('image')){
                    $img = $request->file('image');
                    if(is_null($id)){
                        $file = FileController::uploadFile([
                            "id" => $data->id,
                            "source_des" => 'doctor_profile',
                            "dir" => 'doctors/',
                            "table" => Doctors::class,
                        ], $img);
                    }else{
                        $file = FileController::uploadFile([
                            "update" => $data->profileImage->id,
                            "id" => $data->id,
                            "source_des" => 'doctor_profile',
                            "dir" => 'doctors/',
                            "table" => Doctors::class,
                        ], $img);
                    }

                }
                return redirect()->route('main.doctors')->with(["msg"=>["success", "Doctor created Successfully"]]);
            }else{
                abort(404);
            }

        }

        if(is_null($id)){
            $flag = "Add";
            $route = route('main.doctors-post');
            return view('admin.pages.doctors.post', compact("flag", "route"));
        }else{
            $flag = "Edit";
            $route = route('main.doctors-post', ["id" => $id]);
            $cid = Crypt::decrypt($id);
            $data = Doctors::find($cid);
            return view('admin.pages.doctors.post', compact("flag", "route", "data"));
        }

    }
}
