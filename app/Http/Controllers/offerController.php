<?php

namespace App\Http\Controllers;

use Crypt;
use DataTables;
use App\Model\Offers;
use Illuminate\Http\Request;
use App\Model\Master\FileController;
use Validator;

class offerController extends Controller
{
    public function index (Request $request){
        $doctors = Offers::with("bannerImage");
        if($request->ajax()){
            return DataTables::of($doctors)
                ->addIndexColumn()
                ->editColumn('percentage', function($row){
                    return $row->percentage."%";
                })
                ->editColumn('image', function($row){
                    return "<img class='img-thumbnail' height='50' style='max-width: 100px' src=".asset($row->bannerImage->c_img)." height='50'>";
                })
                ->editColumn('action', function($row){

                    $btn = '<a href="'.route("main.offer.post",["id"=>Crypt::encrypt($row->id)]).'" class="btn btn-warning btn-xs"><i class="fa fa-edit"></></a>';
                    return $btn;

                })
                ->editColumn('status', function($row){
                    if($row->status == "Available"){

                        $status = '<label class="label label-success">Available</label>';

                    }elseif($row->status == "Unavailable"){

                        $status = '<label class="label label-danger">Unavailable</label>';

                    }
                    return $status;

                })
                ->rawColumns(['action', "image", "status"])
                ->make(true);
        }
        return view('admin.pages.offers.index');
    }
    public function offerPost (Request $request, $id = null){
        if($request->isMethod("POST")){

            $rules = [
                "title"  => "required",
                "percentage"  => "required",
                "description"  => "required",
                "status"  => "required",
            ];

            if(is_null($id)){
                $data = new Offers;
                $rules["image"] = "required";
            }else{
                $cid = Crypt::decrypt($id);
                $data = Offers::find($cid);
            }

            $validator = Validator::make($request->all(), $rules);

            if($validator->fails()){
                return back()
                ->withInput()
                ->withErrors($validator);
            }


            $data->title = $request->title;
            $data->percentage = $request->percentage;
            $data->description = $request->description;
            $data->status = $request->status;
            if($data->save()){
                if($request->file('image')){
                    $img = $request->file('image');
                    if(is_null($id)){
                        $file = FileController::uploadFile([
                            "id" => $data->id,
                            "source_des" => 'offer_banner',
                            "dir" => 'offerBanners/',
                            "table" => Offers::class,
                        ], $img);
                    }else{
                        $file = FileController::uploadFile([
                            "update" => $data->bannerImage->id,
                            "id" => $data->id,
                            "source_des" => 'offer_banner',
                            "dir" => 'offerBanners/',
                            "table" => Offers::class,
                        ], $img);
                    }

                }
                return redirect()->route('main.offer.index')->with(["msg"=>["success", "Offer banner has been created Successfully"]]);
            }else{
                abort(404);
            }

        }
        if(is_null($id)){
            $flag = "Add";
            $route = route('main.offer.post');
            return view('admin.pages.offers.post', compact('route', 'flag'));
        }else{
            $cid = Crypt::decrypt($id);
            $flag = "Edit";
            $data = Offers::with("bannerImage")->find($cid);
            $route = route('main.offer.post', ["id" => $id]);
            return view('admin.pages.offers.post', compact('route', 'flag', 'data'));
        }
    }
}
