<?php

namespace App\Http\Controllers;

use Redirect;

use App\contactus;
use App\Model\Doctors;
use Illuminate\Http\Request;
use App\Model\contactUsDetails;
use App\comprehensiveappointment;
use Illuminate\Support\Facades\DB;

class ComprehensiveController extends Controller
{
    public function index()
    {
        return view('website.home');
    }
    public function aboutus()
    {
        return view('website.about');
    }
    public function missionvisionvalues()
    {
        return view('website.mission&vision');
    }
    public function servicesshow()
    {
        return view('website.services');
    }
    public function contactusshow()
    {
        $contactDetails = contactUsDetails::select()->first();
        return view('website.contactus', compact('contactDetails'));
    }

    public function contactcomprehensivesubmit(Request $request)
    {
        $validatecomprehensive = $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'mobile' => 'required',
        ]);

        $savecomprehensivedetails = new contactus;
        $savecomprehensivedetails->firstname = $request->firstname;
        $savecomprehensivedetails->lastname = $request->lastname;
        $savecomprehensivedetails->email = $request->email;
        $savecomprehensivedetails->mobile = $request->mobile;
        $savecomprehensivedetails->additional_information = $request->additional_information;
        $savecomprehensivedetails->save();

        return redirect::back()->with('contactus', 'We have received your message and would like to thank you for writing to us. If your inquiry is urgent, please use the telephone number listed below to talk to one of our staff members. Otherwise, we will reply by email as soon as possible.');
    }

    public function Comprehensiveappointmentnow(Request $request)
    {
        $validateappontment = $request->validate([
            'patient_name' => 'required',
            'mobile_number' => 'required|numeric',
            'email_address' => 'required|email',
            'gender' => 'required',
            'preferred_date' => 'required',
            'preferred_time' => 'required',
        ]);


        $saveappointment = new comprehensiveappointment;
        $saveappointment->patient_name = $request->patient_name;
        $saveappointment->mobile_number = $request->mobile_number;
        $saveappointment->email_address = $request->email_address;
        $saveappointment->gender = $request->gender;
        $saveappointment->doctor = $request->doctor;
        $saveappointment->preferred_date = $request->preferred_date;
        $saveappointment->preferred_time = $request->preferred_time;

        $saveappointment->save();
        return redirect::back()->with('appointment', 'We have received your message and would like to thank you for writing to us. If your inquiry is urgent, please use the telephone number from contact us to talk to one of our staff members. Otherwise, we will reply by email & call as soon as possible.');
    }

    public function doctorsshow()
    {
        $doctors = Doctors::with("profileImage")->where('status', 'Available')->get();
        return view('website.doctors', compact("doctors"));
    }
}
