<?php

namespace App\Model;

use App\Model\Master\FileController;
use Illuminate\Database\Eloquent\Model;

class Doctors extends Model
{
    protected $table = 'doctors';

    public function profileImage()
    {
        return $this->hasOne(FileController::class, 'source_id', 'id')->where("table_name", self::class);
    }
}
