<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class FileController extends Model
{
    protected $table = "file_uploads";

    public static function uploadFile($data, $file){
        // Getting FIle Data
        $fileName = "app".time().rand(10,10000).time().'.'.$file->getClientOriginalExtension();
        $FileExtension = $file->getClientOriginalExtension();
        $FileRealPath = $file->getRealPath();
        $FileSize = $file->getSize();
        $FileMimeType = $file->getMimeType();

        // Storing File
        $destinationPathMain = 'storage/';
        $destinationPathFolder = $data["dir"] ?? 'extra/';
        $destinationPath = $destinationPathMain.$destinationPathFolder;
        $file->move($destinationPath,$fileName);

        // Inserting or Updating Image
        if(isset($data["update"])){
            $fileUpload = FileController::find($data["update"]);
        }else{
            $fileUpload = new FileController;
        }
        $fileUpload->source_id = $data["id"];
        $fileUpload->source_des = $data["source_des"] ?? "";
        $fileUpload->table_name = $data["table"] ?? "";
        $fileUpload->file_name = $fileName;
        $fileUpload->file_type = $FileExtension;
        $fileUpload->file_size = $FileSize;
        $fileUpload->c_img = $destinationPathMain.$destinationPathFolder.$fileName;

        $fileUpload->save();
        return $fileUpload;

        if(!$fileUpload){
            return false;
        }

    }
}
