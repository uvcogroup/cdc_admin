<?php

namespace App\Model;

use App\Model\Master\FileController;
use Illuminate\Database\Eloquent\Model;

class Offers extends Model
{
    protected $table = 'offers';

    public function bannerImage()
    {
        return $this->hasOne(FileController::class, 'source_id', 'id')->where("table_name", self::class);
    }

}
