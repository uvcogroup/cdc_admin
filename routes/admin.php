<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Routing\RouteGroup;

Route::group(["prefix"=>"comprehensive/admin", "as"=>"main."], function(){

    Route::group(["middleware" => "auth"], function () {
        Route::get('/dashboard', 'HomeController@dashboard')->name("dashboard");

        Route::group(['prefix' => 'doctors'], function () {
            Route::get('/', 'doctorsController@index')->name('doctors');
            Route::match(["GET", "POST"], '/post/{id?}', 'doctorsController@post')->name('doctors-post');
        });

        Route::group(['prefix' => 'offers', "as"=>"offer."], function () {
            Route::get('/', 'offerController@index')->name('index');
            Route::match(["GET", "POST"], '/post/{id?}', 'offerController@offerPost')->name('post');
        });

        Route::get('contact-us', 'contactUsController@index')->name('contact-us');
        Route::post('/contact-us-post', 'contactUsController@contactUsPost')->name('contact-us-post');
    });

    Auth::routes();

});

