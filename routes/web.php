<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Routing\RouteGroup;

Route::get('/', 'ComprehensiveController@index')->name('home');
Route::get('about', 'ComprehensiveController@aboutus')->name('about');
Route::get('mission-vision', 'ComprehensiveController@missionvisionvalues')->name('mission&vision');
Route::get('services', 'ComprehensiveController@servicesshow')->name('services');
Route::get('contactus', 'ComprehensiveController@contactusshow')->name('contactus');
Route::post('contactcomprehensive', 'ComprehensiveController@contactcomprehensivesubmit')->name('contactcomprehensive');
Route::post('Comprehensiveappointment', 'ComprehensiveController@Comprehensiveappointmentnow')->name('Comprehensiveappointment');
Route::get('doctors', 'ComprehensiveController@doctorsshow')->name('doctors');
