@extends('website.layout.app')
@section('content')
@include('website.includes.header')
@if ($msg = Session::get('appointment'))
<div class="alert alert-success" role="alert">
        {{$msg}}
</div>
@endif
<div class="container-fluid _cbci">
  <div class="container">
  <div class="row _rm">
    <div class="col-md-6 col-12">
      <div class="_ttd" style=""></div>
        <h2 class="_tcd">Comprehensive Diabetes Center</h2>
        <p class="mt-4">The Comprehensive Diabetes centre opened in March 2008 for
          the purpose of improving the lives of people with diabetes by
          providing a one stop shop where patients could receive clinical care,
          diagnostics and medications at one place.</p>

              <div class=" text-right" style="width:150px; height:100px; float: right; background-color:#F3F8FF">
          </div>
    </div>

    <div class="col-md-1 col-12"></div>
    <div class="col-md-5 col-12">
      <div class="row">
        <div class="col-md-6 col-6 mb-3">

          <div class="_block-services" data-toggle="modal" data-target="#bookanappointment">
          <img src="{{asset('assets/homebackground/appointment.png')}}" style="width:20%" class="img-fluid mb-2" alt="">
            <div>Book an Appointment</div>
          </div>

<!-- Modal -->
      <div class="modal fade" id="bookanappointment" tabindex="-1" role="dialog" aria-labelledby="bookanappointment" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">

              {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button> --}}
            <div class="modal-body">
              <h5 class="">Book your Appointment</h5>
              <div>
              <form action="{{route('Comprehensiveappointment')}}" method="POST">
                {{csrf_field()}}
                  <div class="row">
                    <div class="col-md-6 col-12">
                      <div class="form-group">
                        <label for="First Name">Patient Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control @error('patient_name') is-invalid @enderror" value="{{old('patient_name')}}"  name="patient_name" placeholder="Patient Name" autofocus>
                        @if ($errors->first('patient_name'))
                        <label class="error-msg">{{ $errors->first('patient_name') }}</label>
                        @endif
                      </div>
                    </div>

                    <div class="col-md-6 col-12">
                      <div class="form-group">
                        <label for="Mobile Number">Mobile Number <span class="text-danger">*</span></label>
                        <input type="number" autocomplete="off" maxlength="12" minlength="7"  class="form-control @error('mobile_number') is-invalid @enderror" value="{{old('mobile_number')}}" aria-required="true" aria-invalid="false" name="mobile_number" placeholder="ex. 97150XXXXXXX">
                        @if ($errors->first('mobile_number'))
                        <label class="error-msg">{{ $errors->first('mobile_number') }}</label>
                        @endif
                      </div>
                    </div>

                    <div class="col-md-12 col-12">
                      <div class="form-group">
                        <label for="Email">Email</label>
                        <input type="text" class="form-control @error('email_address') is-invalid @enderror" value="{{old('email_address')}}" name="email_address" placeholder="Email Address">
                        @if ($errors->first('email_address'))
                        <label class="error-msg">{{ $errors->first('email_address') }}</label>
                        @endif
                      </div>
                    </div>



                    <div class="col-md-6 col-12">
                      <div class="form-group">
                        <label for="Gender">Gender <span class="text-danger">*</span></label>
                        <select name="gender" class="form-control @error('gender') is-invalid @enderror">
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                        </select>
                        @if ($errors->first('gender'))
                        <label class="error-msg">{{ $errors->first('gender') }}</label>
                        @endif
                      </div>
                    </div>


                    <div class="col-md-6 col-12">
                      <div class="form-group">
                        <label for="Doctor">Select Doctor</label>
                        <select name="doctor" value="{{old('doctor')}}" class="form-control">
                          <option value="Male">Dr - 1</option>
                          <option value="Female">Dr - 2</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-md-6 col-12">
                      <div class="form-group">

                                <label for="value-specified-js">Preferred Date</label>
                                <input value="{{old('preferred_date')}}" type="text"
                                  id="value-specified"
                                  name="preferred_date"
                                  class="form-control wbn-datepicker @error('preferred_date') is-invalid @enderror"
                                  />
                                  @if ($errors->first('preferred_date'))
                        <label class="error-msg">{{ $errors->first('preferred_date') }}</label>
                        @endif
                                       <script type="text/javascript">
                                        $(function () {
                                          $('.wbn-datepicker').datepicker()

                                          var $jsDatepicker = $('#value-specified-js').datepicker()
                                          $jsDatepicker.val('2025-05-30')
                                        })
                                      </script>

                          </div>
                    </div>

                    <div class="col-md-6 col-12">
                      <div class="form-group">
                        <label for="Preferred Time">Preferred Time</label>
                        <select name="preferred_time" value="{{old('preferred_time')}}" class="form-control @error('preferred_time') is-invalid @enderror">
                          <option value="12.00 AM">12.00 AM</option>
                          <option value="12.30 AM">12.30 AM</option>
                          <option value="01.00 AM">01.00 AM</option>
                          <option value="01.30 AM">01.30 AM</option>
                          <option value="02.00 AM">02.00 AM</option>
                          <option value="02.30 AM">02.30 AM</option>
                          <option value="03.00 AM">03.00 AM</option>
                          <option value="03.30 AM">03.30 AM</option>
                          <option value="04.00 AM">04.00 AM</option>
                          <option value="04.30 AM">04.30 AM</option>
                          <option value="05.00 AM">05.00 AM</option>
                          <option value="05.30 AM">05.30 AM</option>
                          <option value="06.00 AM">06.00 AM</option>
                          <option value="06.30 AM">06.30 AM</option>
                          <option value="07.00 AM">07.00 AM</option>
                          <option value="07.30 AM">07.30 AM</option>
                          <option value="08.00 AM">08.00 AM</option>
                          <option value="08.30 AM">08.30 AM</option>
                          <option value="09.00 AM">09.00 AM</option>
                          <option value="09.30 AM">09.30 AM</option>
                          <option value="10.00 AM">10.00 AM</option>
                          <option value="10.30 AM">10.30 AM</option>
                          <option value="11.00 AM">11.00 AM</option>
                          <option value="11.30 AM">11.30 AM</option>
                          <option value="12.00 PM">12.00 PM</option>
                          <option value="12.30 PM">12.30 PM</option>
                          <option value="01.00 PM">01.00 PM</option>
                          <option value="01.30 PM">01.30 PM</option>
                          <option value="02.00 PM">02.00 PM</option>
                          <option value="02.30 PM">02.30 PM</option>
                          <option value="03.00 PM">03.00 PM</option>
                          <option value="03.30 PM">03.30 PM</option>
                          <option value="04.00 PM">04.00 PM</option>
                          <option value="04.30 PM">04.30 PM</option>
                          <option value="05.00 PM">05.00 PM</option>
                          <option value="05.30 PM">05.30 PM</option>
                          <option value="06.00 PM">06.00 PM</option>
                          <option value="06.30 PM">06.30 PM</option>
                          <option value="selected" selected="">07.00 PM</option>
                          <option value="07.30 PM">07.30 PM</option>
                          <option value="08.00 PM">08.00 PM</option>
                          <option value="08.30 PM">08.30 PM</option>
                          <option value="09.00 PM">09.00 PM</option>
                          <option value="09.30 PM">09.30 PM</option>
                          <option value="10.00 PM">10.00 PM</option>
                          <option value="10.30 PM">10.30 PM</option>
                          <option value="11.00 PM">11.00 PM</option>
                          <option value="11.30 PM">11.30 PM</option>
                      </select>
                      @if ($errors->first('preferred_time'))
                      <label class="error-msg">{{ $errors->first('preferred_time') }}</label>
                      @endif
                    </div>
                    </div>

                    <div class="form-group">
                      <div class="col-md-12 col-12">
                      <button type="submit" class="btn btn-dark btn-sm btn-block">Book An Appointment</button>
                    </div>
                    </div>


                  </div>

                </form>
              </div>
            </div>

          </div>
        </div>
      </div>
        </div>

        <div class="col-md-6 col-6 mb-3">
        <a href="{{route('doctors')}}">
          <div class="_block-services">
          <img src="{{asset('assets/homebackground/searchadoctor.png')}}" style="width:20%" class="img-fluid mb-2" alt="">
              <div>
              Find A Doctor
            </div>
            </div>
          </a>
          </div>

          <div class="col-md-6 col-6 mb-3">
              <div class="_block-services">
          <img src="{{asset('assets/homebackground/offers.png')}}" style="width:20%" class="img-fluid mb-2" alt="">
          <div>
                Comprehensive Offers
              </div>
              </div>
            </div>

            <div class="col-md-6 col-6 mb-3">
            <a href="{{route('contactus')}}">
              <div class="_block-services">
          <img src="{{asset('assets/homebackground/contactus.png')}}" style="width:20%" class="img-fluid mb-2" alt="">
          <div>
                  Contact Us
                </div>
                </div>
              </a>
              </div>
      </div>
    {{-- <img src="{{asset('assets/homebackground/comprehensivedoctors.png')}}" style="width:80%" class="img-fluid" alt=""> --}}
    </div>


  </div>

</div>
</div>

<div class="container-fluid _cfcs">
  <div class="container">



  <h2 class="_tcd"><div class="_ttd " style=""></div>Comprehensive Clinic Services</h2>
  <div class="row" style="margin-top:50px">
    <div class="col-md-4 col-12 mb-4">
      <div>
        <div>
      <img src="{{asset('assets/homebackground/education.png')}}" class="img-fluid _iso"  alt="">
    </div>
      </div>
      <div class="mt-3">
        <label for="Diabetes Education">Diabetes Education</label>
      </div>
      <div>
        <small class="text-secondary block-ellipsis">
            Diabetes training includes an introduction to insulin and
            diabetes.It teaches how the body controls blood glucose concentrations,How
            insulin works and what happens in diabetes,Types of insulin available for clinical use,
            Clinical indications for using insulin,Administration of insulin, Factors affecting insulin
            performanceand The side effects of insulin.
        </small>
      </div>
    </div>


    <div class="col-md-4 col-12 mb-4">
        <div>
          <div>
        <img src="{{asset('assets/homebackground/heartcheck.png')}}" class="img-fluid _iso" alt="">
      </div>
        </div>
        <div class="mt-3">
          <label for="Diabetes Education">Outpatient clinical services</label>
        </div>
        <div>
          <small class="text-secondary block-ellipsis">
              Outpatient cash and insurance caver. Outpatient care covers treatment
              that does not require a day admission or an overnight stay at a our medical clinic.
              As you're generally not taking up as much of the hospital's time like you would with
              inpatient care, the treatments and processes are generally affordable under our doctors.
          </small>
        </div>
      </div>


      <div class="col-md-4 col-12 mb-4">
          <div>
            <div>
          <img src="{{asset('assets/homebackground/clinicalservices.png')}}" class="img-fluid _iso" style="" alt="">
        </div>
          </div>
          <div class="mt-3">
            <label for="Diabetes Education">Inpatient clinical services</label>
          </div>
          <div>
            <small class="text-secondary block-ellipsis">
                This treatment center provides a safe and secure place for patients to undergo more
                intensive treatment than outpatient care can offer. Within the inpatient environment,
                daily schedules are predetermined for patients who must follow them without deviation under
                the care of our able specialist within the referred hospital around Aga Khan, Avenue Hospital and MP shah.
            </small>
          </div>
        </div>

        <div class="col-md-4 col-12 mb-4">
            <div>
              <div>
            <img src="{{asset('assets/homebackground/Insulinpump.png')}}" class="img-fluid _iso" style="" alt="">
          </div>
            </div>
            <div class="mt-3">
              <label for="Diabetes Education">Insulin pump Centre</label>
            </div>
            <div>
              <small class="text-secondary block-ellipsis">
                  We offer affordable insulin pumps and support, download and interpreted the data.
                  For Medtronic insulin pumps, we offer consummeable and training on how to use it.
              </small>
            </div>
          </div>

          <div class="col-md-4 col-12 mb-4">
              <div>
                <div>
              <img src="{{asset('assets/homebackground/Diagnostics.png')}}" class="img-fluid _iso" style="" alt="">
            </div>
              </div>
              <div class="mt-3">
                <label for="Diabetes Education">Diagnostics</label>
              </div>
              <div>
                <small class="text-secondary block-ellipsis">
                    The diagnostic include continuous glucose monitoring, Foot pressure Measurement,
                    Peripheral autonomic nervous system screening, Peripheral neuropathy screening,
                    Glucose measuring and 24 hrs blood pressure monitoring.
                </small>
              </div>
            </div>

  </div>

</div>
</div>


<div class="container mt-4 mb-4">
  <div class="row">
    <div class="col-md-7 col-12">
        <div style="margin:50px 0px 50px 0px">
            <h2 class="_tcd"><div class="_ttd " style=""></div>Quality Service</h2>
            <div class="mt-4">
              <p>
                  First and only facility in Kenya that provides insulin pump training and support in terms
              </p>
              <p>
                  Our services are centered towards managing and treatment of all levels of diabetes through patient treatment and education
                  Below are the services we offer.
              </p>
            </div>
          </div>
    </div>
    <div class="col-md-1 col-12"></div>
    <div class="col-md-4 col-12 text-center mt-4">
        <div>
        <img src="{{asset('assets/homebackground/signup.png')}}" class="img-fluid mt-1" style="width:50%">
        <div>
          <h5>Stay tuned !</h5>
        </div>
        <div>
          <small>Subscribe our newsletter and get notifications to stay update</small>
        </div>
        <div style="margin:0px 0px 0px 0px">
            <div>
              <div class="form-group" style="position:relative">
                <input type="text" class="form-control" placeholder="Subscribe Us">
                <div style="position:absolute; top:13px; right:10px">
                  <button class="btn btn-dark btn-sm">subscribe</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
  </div>

  </div>
<script>
$('.wbn-datepicker').datepicker()

</script>


@include('website.includes.footer')
@endsection
