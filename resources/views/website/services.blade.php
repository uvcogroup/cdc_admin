@extends('website.layout.app')
@section('content')
@include('website.includes.header')

<div class="container mt-4">
        <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Services</li>
                </ol>
              </nav>
    <div class="row">
        <div class="col-md-6 col-12">
  <h2 class="_tcd"><div class="_ttd " style=""></div>Comprehensive Clinic Services</h2>
  <label for="Diabetes Education">Diabetes Education</label>

<p>
    Diabetes training includes an introduction to insulin and diabetes.It teaches how the body
    controls blood glucose concentrations,How insulin works and what happens in diabetes,Types
    of insulin available for clinical use, Clinical indications for using insulin,Administration
    of insulin, Factors affecting insulin performanceand The side effects of insulin.


</p>

<label for="Outpatient clinical services">Outpatient clinical services</label>

<p>
    Outpatient cash and insurance caver. Outpatient care covers treatment that
    does not require a day admission or an overnight stay at a our medical clinic.
    As you're generally not taking up as much of the hospital's time like you would
    with inpatient care, the treatments and processes are generally affordable under our doctors.

</p>

<label for="Inpatient clinical services">Inpatient clinical services</label>

<p>
    This treatment center provides a safe and secure place for patients to undergo more
    intensive treatment than outpatient care can offer. Within the inpatient environment,
    daily schedules are predetermined for patients who must follow them without deviation
    under the care of our able specialist within the referred hospital around Aga Khan,
    Avenue Hospital and MP shah.

</p>

<label for="Insulin pump Centre">Insulin pump Centre</label>

<p>
    We offer affordable insulin pumps and support, download and interpreted the data.
    For Medtronic insulin pumps, we offer consummeable and training on how to use it.



</p>

<label for="Diagnostics">Diagnostics</label>

<p>
    The diagnostic include continuous glucose monitoring, Foot pressure Measurement,
    Peripheral autonomic nervous system screening, Peripheral neuropathy screening, Glucose
    measuring and 24 hrs blood pressure monitoring.
</p>
        </div>

        <div class="col-md-6 col-12 text-center">
        <img src="{{asset('assets/homebackground/services.png')}}" class="img-fluid" style="width:50%" alt="">
        </div>
    </div>
</div>
@include('website.includes.footer')
@endsection
