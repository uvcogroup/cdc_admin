@extends('website.layout.app')
@section('content')
@include('website.includes.header')

<div class="container mt-4">
        <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Mission Vision & Values</li>
                </ol>
              </nav>
    <div class="row">
        <div class="col-md-6 col-12">
  <h2 class="_tcd"><div class="_ttd " style=""></div>Mission Vision & Values</h2>
<p>
    To positively contribute to health and well being of persons living with Diabetes
    by providing a one-stop shop where patients could receive clinical care,
    diagnostics and medications at one place.
</p>
        </div>

        <div class="col-md-6 col-12 text-center">
        <img src="{{asset('assets/homebackground/misionvisionandvalues.png')}}" class="img-fluid" style="width:50%" alt="">
        </div>
    </div>
</div>
@include('website.includes.footer')
@endsection
