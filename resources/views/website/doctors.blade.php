@extends('website.layout.app')
@section('content')
@include('website.includes.header')
<div class="container mt-4">
        <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Doctors</li>
                </ol>
              </nav>
            </div>
<div class="container _ccu text-center">

  <h2 class="_tcd"><div class="_ttd " style=""></div>Contact Comprehensive Diabetes Doctors</h2>
  <p>
        We’re here to help and answer any question you might have. We look forward to hearing from you 🙂

  </p>
</div>
<div class="container mt-4 mb-4">
    <div class="row">
        @foreach ($doctors as $item)
            <div class="col-md-3 col-12 mb-3">
                <div>
                    <div class="text-center _cpb">
                        <img src="@if($item->profileImage) {{ asset($item->profileImage->c_img) }} @else {{ asset('assets/img/default-doctor.jpg') }} @endif" class="mt-3" style="width:150px; height:150px; border-radius:50%" alt="">
                    <div class="mt-2">
                        <label for="Dr. Farah Sherdel">{{ $item->name }}</label>
                    </div>
                    </div>

                </div>
            </div>
        @endforeach
    </div>
</div>
@include('website.includes.footer')
@endsection
