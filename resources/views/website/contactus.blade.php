@extends('website.layout.app')
@section('content')
@include('website.includes.header')
<div class="container mt-4">
<nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
        </ol>
      </nav>
    </div>
<div class="container _ccu text-center">
        @if ($msg = Session::get('contactus'))
        <div class="alert alert-success" role="alert">
                {{$msg}}
        </div>
        @endif
    <div>
        <label for="Question">GOT A QUESTION ?</label>
    </div>
  <h2 class="_tcd"><div class="_ttd " style=""></div>Contact Comprehensive Diabetes Center</h2>
  <p>
        We’re here to help and answer any question you might have. We look forward to hearing from you 🙂

  </p>
</div>

<div class="container">
<form action="{{route('contactcomprehensive')}}" method="POST">
        {{csrf_field()}}

 <div class="row">
     <div class="col-md-6 col-12">
        <div>
            <div class="row">
                <div class="col-md-6 col-12">
                        <div class="form-group">
                                <label for="First Name">First Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('firstname') is-invalid @enderror" name="firstname" value="{{old('firstname')}}"  placeholder="First Name">
                                @if ($errors->first('firstname'))
                                <label class="error-msg">{{ $errors->first('firstname') }}</label>
                                @endif
                            </div>
                </div>

                <div class="col-md-6 col-12">
                        <div class="form-group">
                                <label for="First Name">Last Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{old('lastname')}}"   placeholder="Last Name">
                                @if ($errors->first('lastname'))
                                <label class="error-msg">{{ $errors->first('lastname') }}</label>
                                @endif
                            </div>
                </div>

                <div class="col-md-12 col-12">
                        <div class="form-group">
                                <label for="Email">Email <span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{old('email')}}"   placeholder="Email">
                                @if ($errors->first('email'))
                                <label class="error-msg">{{ $errors->first('email') }}</label>
                                @endif
                </div>
                </div>

                <div class="col-md-12 col-12">
                        <div class="form-group">
                                <label for="Mobile">Mobile <span class="text-danger">*</span></label>
                                <input type="number" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{old('mobile')}}"   placeholder="Mobile">
                                @if ($errors->first('mobile'))
                                <label class="error-msg">{{ $errors->first('mobile') }}</label>
                                @endif
                </div>
                </div>
                <div class="col-md-12 col-12">
                        <div class="form-group">
                                <label for="Additional Details">Additional Details</label>
                                <textarea class="form-control" name="additional_information" placeholder="Additional Details"></textarea>
                </div>
                </div>

                <div class="col-md-12 col-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-dark btn-block">Send Message</button>
                        </div>
                </div>

            </div>

        </div>
     </div>
     <div class="col-md-6 col-12">
  <h5 class="_tcd"><div class="_ttd " style=""></div>Contact Information</h5>

                @foreach (json_decode($contactDetails->address) as $item)
                    <div>
                        <label for="">{{ $item }}</label>
                    </div>
                @endforeach
                <div>
                    <small>
                        <strong>Phone:</strong>&nbsp;
                        @php
                            $pcount = 0;
                        @endphp
                        @foreach (json_decode($contactDetails->phone_numbers) as $item)
                            @php
                                $pcount++;
                                if($pcount < count(json_decode($contactDetails->phone_numbers))){
                                    $phtml = ",";
                                }else{
                                    $phtml = '';
                                }
                            @endphp
                            <a href="tel:+254 {{ $item }}"><strong>+254</strong> {{ $item }}</a> {{ $phtml }}
                        @endforeach
                    </small>
                </div>
                <div>
                    <small>
                        <strong>Email: </strong>&nbsp;
                        @php
                            $count = 0;
                        @endphp
                        @foreach (json_decode($contactDetails->emails) as $item)
                            @php
                                $count++;
                                if($count < count(json_decode($contactDetails->emails))){
                                    $html = "or";
                                }else{
                                    $html = '';
                                }
                            @endphp
                            <strong><a href="mailto:{{ $item }}">{{ $item }}</a></strong> {{ $html }}
                        @endforeach
                    </small>
                </div><br>
            <iframe src="{{ $contactDetails->map_url }}" style="width:100%; height:250px" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
 </div>
</form>
</div>

@include('website.includes.footer')
@endsection
