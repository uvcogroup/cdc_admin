@extends('website.layout.app')
@section('content')
@include('website.includes.header')

<div class="container mt-4">
        <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">About Us</li>
                </ol>
              </nav>
    <div class="row">
        <div class="col-md-6 col-12">
  <h2 class="_tcd"><div class="_ttd " style=""></div>About Comprehensive Clinic</h2>
<p>
    Comprehensive Diabetes centre will be recognized as a leader in Diabetes
    care and innovation in Nairobi and Kenya .The centre will be characterized
    by the excellence of its health care services, its focus use of effective
    innovative technologies with a culture of continuous quality improvement.
</p>
        </div>

        <div class="col-md-6 col-12 text-center">
        <img src="{{asset('assets/homebackground/aboutus.png')}}" class="img-fluid" style="width:50%" alt="">
        </div>
    </div>
</div>
@include('website.includes.footer')
@endsection
