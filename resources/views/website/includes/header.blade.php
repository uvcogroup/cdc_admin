
  <div class="nav">
        <input type="checkbox" id="nav-check">
        <div class="nav-header">
          <div class="nav-title">
          <a href="{{url('/')}}"><img src="{{asset('assets/logo/logo.png')}}" style="width:50px"></a>
          </div>
        </div>
        <div class="nav-btn">
          <label for="nav-check">
            <span></span>
            <span></span>
            <span></span>
          </label>
        </div>

        <div class="nav-links">
        <a href="{{route('about')}}">About Us</a>
        <a href="{{route('mission&vision')}}">Mission Vision & Values</a>
        <a href="{{ route('doctors') }}">Patients & Visitors</a>
          <a href="{{route('services')}}">Services</a>
        <a href="{{route('contactus')}}">Contact Us</a>
        </div>
      </div>
