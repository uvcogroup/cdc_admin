
  <footer class="blog-footer">
    <div class="_ttd " style=""></div>
    <small>© COMPREHENSIVE DIABETES CENTER. ALL RIGHTS RESERVED | DESIGN BY CAREPRO</small>
    <p>
      <a href="#" class="">Back to top</a>
    </p>
    <div class="_ttd " style=""></div>
  </footer>

  <script src="{{ asset("assets/plugins/jquery-3.3.1/jquery.min.js") }}"></script>
  <script src="{{asset('assets/js/comprehensive.js')}}"></script>
  <script src="{{asset('assets/js/wbn-datepicker.js')}}"></script>
  <script>
    currentURL = '{{ url()->current() }}';

    $.fn.sidebarActive = (route) => {
        $.each($('.nav>.nav-links>a'), function(ii, ele){
            if($(ele).attr('href') == route){
                $(ele).addClass('active')
            }
        });
    }

    $.fn.sidebarActive(currentURL);
  </script>
