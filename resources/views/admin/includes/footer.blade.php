
	<!-- page-content" -->
	<div class="clearfix"></div>
</div>
<!-- page-wrapper -->
<!-- Jquery Link -->
<script src="{{ asset("assets/plugins/jquery-3.3.1/jquery.min.js") }}"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="{{ asset("assets/js/bootstrap.min.js") }}"></script>
<!-- Readmore.js-master -->
<script src="{{ asset("assets/plugins/Readmore.js-master/readmore.js") }}"></script>

<!-- Datatable -->
<script src="{{ asset("assets/plugins/DataTables/datatables.min.js") }}"></script>

<!-- Datatable Buttons -->
{{-- <script src="{{ asset("assets/plugins/DataTables/Buttons-1.4.2/js/dataTables.buttons.min.js") }}"></script>
<script src="{{ asset("assets/plugins/DataTables/Buttons-1.4.2/js/buttons.html5.min.js") }}"></script>
<script src="{{ asset("assets/plugins/DataTables/Buttons-1.4.2/js/buttons.print.min.js") }}"></script>
<script src="{{ asset("assets/plugins/DataTables/Buttons-1.4.2/js/buttons.flash.min.js") }}"></script>
<script src="{{ asset("assets/plugins/DataTables/Buttons-1.4.2/js/buttons.colVis.min.js") }}"></script>
<script src="{{ asset("assets/plugins/DataTables/pdfmake-0.1.32/pdfmake.min.js") }}"></script>
<script src="{{ asset("assets/plugins/DataTables/pdfmake-0.1.32/vfs_fonts.js") }}"></script>
<script src="{{ asset("assets/plugins/DataTables/JSZip-2.5.0/jszip.min.js") }}"></script> --}}

<!-- Select2  -->
<script type="text/javascript" src="{{ asset("assets/plugins/select2/dist/js/select2.full.min.js") }}"></script>

{{-- Jquery Validaton --}}
<script type="text/javascript" src="{{ asset("assets/plugins/jquery-validation-master/jquery.validate.min.js") }}"></script>

{{-- DateRangePicker --}}
<script src="{{ asset("assets/plugins/daterangepicker-master/moment.min.js") }}"></script>
<script src="{{ asset("assets/plugins/daterangepicker-master/daterangepicker.js") }}"></script>

{{-- Sweet Alert --}}
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert2-master/sweetalert2.min.css') }}">
<script src="{{ asset('assets/plugins/sweetalert2-master/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-validation-master/jquery.validate.min.js') }}"></script>

@if($msg = Session::get('msg'))
	@php
		if($msg[0] == "success"){
			$title = "Good Job!";
		}else if($msg[0] == "error"){
			$title = "Error!";
		}
	@endphp
@endif

<script>

    $.validator.setDefaults({
        errorElement: 'label',
        errorClass: 'error',
        errorPlacement: function (error, element) {
            if (element.parents('.input-group').length) {
                error.insertAfter(element.parents('.input-group'));
            } else if (element.data('select2')) {
                if(element.parent().hasClass("form-group")){
                    element.parent('.form-group').append(error);
                }else if(element.parent('div')){
                    element.parent('div').append(error);
                }
            }else{
                error.insertAfter(element);
            }
        }
    });

	$(document).ready(function() {
		$("body").removeClass("preload");
	});

	@if($msg = Session::get('msg'))
		Swal.fire(
			"{{ $title }}",
			"{{ $msg[1] }}",
			"{{ $msg[0] }}"
		)
	@endif

	currentURL = '{{ url()->full() }}';

	$.fn.SidebarActive = function($value){
		$('.tadbeer-theme .sidebar-wrapper .sidebar-menu ul li a').each(function(){
			if($(this).attr('href') == $value){
				if($(this).parents().hasClass('sidebar-dropdown')){
					$(this).closest('.sidebar-dropdown').addClass('active');
					$(this).parent('li').addClass('active')
					$(this).closest('.sidebar-dropdown').find('.sidebar-submenu').show();
				}else{
					$(this).parent('li').addClass('active')
				}
			}
		});
	}

	$.fn.SidebarActive(currentURL);
    $.each($('form'), function(ii, ele) {
        $(ele).attr('autocomplete', 'off');
    })
</script>

<script src="{{ asset('assets/js/custom.js') }}"></script>
@stack('footer-js')
</body>
</html>
