<!-- Notification-content  -->
<div class="sidebar-footer">
    <a href="#">
        <i class="fa fa-bell"></i>
        <!-- <span class="badge badge-pill badge-warning notification">3</span> -->
    </a>
    <a href="#">
        <i class="fa fa-envelope"></i>
        <!-- <span class="badge badge-pill badge-success notification">7</span> -->
    </a>
    <a href="">
        <i class="fa fa-cog"></i>
        <span class="badge-sonar"></span>
    </a>
    <a href="{{ route('main.logout') }}"
    onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
        <i class="fa fa-power-off"></i>
    </a>
    <form id="logout-form" action="{{ route('main.logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</div>
