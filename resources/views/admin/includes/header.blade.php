<!DOCTYPE html>
<html>
	<head>
		<title>Comprehensive Diabetes Center | @yield('title') </title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		{{-- <link rel="shortcut icon" type="image/png" href="{{ asset("assets/img/icons/fav-icon.png") }}"/> --}}
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="{{ asset("assets/css/bootstrap.min.css") }}">
		<!-- Optional theme -->
		<link rel="stylesheet" href="{{ asset("assets/css/bootstrap-theme.min.css") }}">
		<link rel="stylesheet" href="{{ asset("assets/css/theme-style.min.css") }}">
		<link rel="stylesheet" href="{{ asset("assets/css/style.min.css") }}">
		<link rel="stylesheet" href="{{ asset("assets/css/responsive.min.css") }}">
		<!-- Font Awesome 5 -->
		<link href="{{ asset("assets/plugins/fontawesome-5.8.1/css/all.css") }}" rel="stylesheet">

		<!-- DataTables -->
		<link rel="stylesheet" href="{{ asset("assets/plugins/DataTables/datatables.min.css") }}">

		<!-- Open+Sans font-->
		<link href="{{ asset("assets/fonts/custom-font.css") }}" rel="stylesheet">
		<!-- Select 2 -->
		<link href="{{ asset("assets/plugins/select2/dist/css/select2.min.css") }}" rel="stylesheet">

	</head>
	<body class="preload">
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
				</nav>
				<div class="page-wrapper tadbeer-theme toggled">
					<a id="show-sidebar" class="btn btn-sm btn-dark">
						<i class="fas fa-bars"></i>
					</a>
					<nav id="sidebar" class="sidebar-wrapper">
						<div class="sidebar-content">
							<div class="sidebar-brand">
								<a href="{{ route("main.dashboard") }}">
									<h1>Comprehensive Diabetes Center</h1>
								</a>
								<div id="close-sidebar">
									<i class="fas fa-toggle-on"></i>
								</div>
							</div>
							<div class="sidebar-header">
								<div class="user-pic">
									<img class="img-responsive img-rounded" src="{{ asset("assets/img/icons/avatar-male.jpg") }}" alt="User picture">
								</div>
								<div class="user-info">
									<span class="user-name">{{ Auth::user()->name }}
									</span>
									<span class="user-role">Administrator</span>
									<span class="user-status">
										<i class="fa fa-circle"></i>
										<span>Online</span>
									</span>
								</div>
							</div>
                            @include('admin.includes.notifications')
							<!-- sidebar-search  -->
							<div class="sidebar-menu">
								<ul>
									<li class="header-menu">
										<span>General</span>
									</li>
									<li>
										<a href="{{ route("main.dashboard") }}">
											<i class="fa fa-tachometer-alt"></i>
											<span>Dashboard</span>
										</a>
									</li>
									<li class="header-menu">
										<span>Others</span>
									</li>
									<li>
										<a href="{{ route('main.contact-us') }}">
											<i class="fa fa-phone"></i>
											<span>Update Contact Us</span>
										</a>
									</li>
									<li>
										<a href="{{ route('main.doctors') }}">
											<i class="fa fa-users"></i>
											<span>Doctors</span>
										</a>
									</li>
                                    <li>
                                        <a href="{{ route('main.offer.index') }}">
											<i class="fa fa-bell"></i>
											<span>Offers</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</nav>
