<?php include('includes/header.php') ?>
<!-- Dashbaord Page -->
<main class="page-content">
	<div class="content-header">
		<div class="page-detail">
			<h2 class="page-heading">Elements</h2>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fas fa-home"></i></a></li>
				<li class="active">Elements</li>
			</ol>
		</div>
	</div>
	<div class="page-inner filter-page-inner">
		<label data-toggle="collapse" href="#collapseExample" role="button" class="label label-default header-filter-label" id="toggle-caret">
			<i class="fas fa-filter"></i> Filter &nbsp; <i class="fas fa-caret-down caret-icon"></i>
		</label>
		<div class="collapse in" id="collapseExample">
			<div  class="card">
				<div class="card-header">
					<h5>Form Elements</h5>
				</div>
				<div class="card-block">
					<form action="">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<label for="">Text Box</label>
									<input type="text" class="form-control" placeholder="Text Here">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="">Select Box</label>
									<select name="" class="form-control" id="">
										<option selected="true" disabled="disabled">Select</option>
										<option value="asdasd">One</option>
										<option value="asdasd">Two</option>
										<option value="asdasd">Three</option>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="">Select-2 Plugin Box</label>
									<select name="select2" class="form-control" id="select2">
										<option value=""></option>
										<option value="asdasd">One</option>
										<option value="asdasd">Two</option>
										<option value="asdasd">Three</option>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="">Select-2 Multiple Plugin Box</label>
									<select name="select2-multiple" multiple="" class="form-control" id="select2-multiple">
										<option value="asda">One</option>
										<option value="asdasd">Two</option>
										<option value="asdasd">Three</option>
									</select>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-1">
								<label class="check-box">One
									<input type="checkbox" checked="checked">
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="col-md-1">
								<label class="radio-box">One
									<input type="radio" name="radio">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header">
				<h5>Bootstrap Tabs</h5>
			</div>
			<div class="card-block">
				<div class="row">
					<div class="col-md-6">
                        <div class="sub-title">Nav Tabs</div>
                        <!-- Nav tabs -->
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#home">Home</a></li>
							<li><a data-toggle="tab" href="#menu1">Menu 1</a></li>
							<li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
							<li><a data-toggle="tab" href="#menu3">Menu 3</a></li>
						</ul>
						<div class="tab-content">
							<div id="home" class="tab-pane fade in active">
							</div>
							<div id="menu1" class="tab-pane fade">
							</div>
							<div id="menu2" class="tab-pane fade">
							</div>
							<div id="menu3" class="tab-pane fade">
							</div>
						</div>
                    </div>
					<div class="col-md-6">
                        <div class="sub-title">Nav Pills</div>
                        <!-- Nav Pilss -->
						<ul class="nav nav-pills">
							<li class="active"><a data-toggle="pill" href="#home-pills">Home</a></li>
							<li><a data-toggle="pill" href="#menu1-pills">Menu 1</a></li>
							<li><a data-toggle="pill" href="#menu2-pills">Menu 2</a></li>
							<li><a data-toggle="pill" href="#menu3-pills">Menu 3</a></li>
						</ul>

						<div class="tab-content">
							<div id="home-pills" class="tab-pane fade in active">
							</div>
							<div id="menu1-pills" class="tab-pane fade">
							</div>
							<div id="menu2-pills" class="tab-pane fade">
							</div>
							<div id="menu3-pills" class="tab-pane fade">
							</div>
						</div>
                    </div>
                    <div class="clearfix"></div>
                    <br>
					<div class="col-md-6">
                        <div class="sub-title">Center Nav Tabs</div>
                        <!-- Nav tabs -->
						<ul class="nav nav-tabs tabs-center">
							<li class="active"><a data-toggle="tab" href="#home-tabs-center">Home</a></li>
							<li><a data-toggle="tab" href="#menu1-tabs-center">Menu 1</a></li>
							<li><a data-toggle="tab" href="#menu2-tabs-center">Menu 2</a></li>
							<li><a data-toggle="tab" href="#menu3-tabs-center">Menu 3</a></li>
						</ul>
						<div class="tab-content">
							<div id="home-tabs-center" class="tab-pane fade in active">
							</div>
							<div id="menu1-tabs-center" class="tab-pane fade">
							</div>
							<div id="menu2-tabs-center" class="tab-pane fade">
							</div>
							<div id="menu3-tabs-center" class="tab-pane fade">
							</div>
						</div>
                    </div>
					<div class="col-md-6">
                        <div class="sub-title">Center Nav Pills</div>
                        <!-- Nav Pilss -->
						<ul class="nav nav-pills tabs-center">
							<li class="active"><a data-toggle="pill" href="#home-pills-center">Home</a></li>
							<li><a data-toggle="pill" href="#menu1-pills-center">Menu 1</a></li>
							<li><a data-toggle="pill" href="#menu2-pills-center">Menu 2</a></li>
							<li><a data-toggle="pill" href="#menu3-pills-center">Menu 3</a></li>
						</ul>

						<div class="tab-content">
							<div id="home-pills-center" class="tab-pane fade in active">
							</div>
							<div id="menu1-pills-center" class="tab-pane fade">
							</div>
							<div id="menu2-pills-center" class="tab-pane fade">
							</div>
							<div id="menu3-pills-center" class="tab-pane fade">
							</div>
						</div>
                    </div>
				</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header">
				<h5>Text Divider</h5>
			</div>
			<div class="card-block">
				<div class="divider-text mb-20">
					<span>Sample Heading</span>
				</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header">
				<h5>Buttons</h5>
			</div>
			<div class="card-block">
				<button class="btn btn-success">Button Success</button>
				<button class="btn btn-primary">Button Primary</button>
				<button class="btn btn-info">Button Info</button>
				<button class="btn btn-danger">Button Danger</button>
				<button class="btn btn-warning">Button Warning</button>
				<button class="btn theme-button">Theme Button</button>
			</div>
		</div>
		<div class="card">
			<div class="card-header">
				<h5>Panels</h5>
			</div>
			<div class="card-block">

			</div>
		</div>
	</div>
</main>
<?php include('includes/footer.php') ?>
<script>
	$(document).ready(function() {
		$('#select2').select2({
			placeholder: 'Select',
		})
		$('#select2-multiple').select2({
			placeholder: 'Select',
		})

	} );
</script>
