<?php include('includes/header.php') ?>
<!-- Dashbaord Page -->
<main class="page-content">
	<div class="content-header">
		<div class="page-detail col-md-8">
			<h2 class="page-heading">Company Setup</h2>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fas fa-home"></i></a></li>
				<li class="active">Company Setup</li>
			</ol>
		</div>
		<div class="page-detail col-md-4 flex-right text-right">
			<a href="add-detail.php" class="btn btn-theme first-header-button"> <i class="fas fa-plus"></i> Add Page</a>
		</div>
	</div>
	<div class="page-inner filter-page-inner">
		<label data-toggle="collapse" href="#collapseExample" role="button" class="label label-default header-filter-label" id="toggle-caret">
			<i class="fas fa-filter"></i> Filter &nbsp; <i class="fas fa-caret-down caret-icon"></i>
		</label>
		<div class="collapse" id="collapseExample">
			<div class="card">
				<div class="card-block">
					<form action="">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<label for="">Select-2</label>
									<select class="form-control select2">
										<option value=""></option>
										<option value="asdasd">One</option>
										<option value="asdasd">Two</option>
										<option value="asdasd">Three</option>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="">Select-2</label>
									<select class="form-control select2">
										<option value=""></option>
										<option value="asdasd">One</option>
										<option value="asdasd">Two</option>
										<option value="asdasd">Three</option>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="">Select-2</label>
									<select class="form-control select2">
										<option value=""></option>
										<option value="asdasd">One</option>
										<option value="asdasd">Two</option>
										<option value="asdasd">Three</option>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="">Select-2</label>
									<select class="form-control select2">
										<option value=""></option>
										<option value="asdasd">One</option>
										<option value="asdasd">Two</option>
										<option value="asdasd">Three</option>
									</select>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="card">
			<div class="card-block">
				<table id="example" class="display table-condensed action-table" style="width:100%">
					<thead>
						<tr>
							<th>Name</th>
							<th>Position</th>
							<th>Office</th>
							<th>Age</th>
							<th>Start date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Tiger Nixon</td>
							<td>System Architect</td>
							<td>Edinburgh</td>
							<td>61</td>
							<td>2011/04/25</td>
							<td>
								<div class="btn-group" role="group" aria-label="...">
									<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-info btn-xs"><i class="fas fa-eye"></i></button>
									<a href="company-manage.php" type="button" class="btn btn-warning btn-xs"><i class="fas fa-cog"></i></a>
								</div>
							</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>System Architect</td>
							<td>Edinburgh</td>
							<td>61</td>
							<td>2011/04/25</td>
							<td>
								<div class="btn-group" role="group" aria-label="...">
									<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-info btn-xs"><i class="fas fa-eye"></i></button>
									<a href="company-manage.php" type="button" class="btn btn-warning btn-xs"><i class="fas fa-cog"></i></a>
								</div>
							</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>System Architect</td>
							<td>Edinburgh</td>
							<td>61</td>
							<td>2011/04/25</td>
							<td>
								<div class="btn-group" role="group" aria-label="...">
									<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-info btn-xs"><i class="fas fa-eye"></i></button>
									<a href="company-manage.php" type="button" class="btn btn-warning btn-xs"><i class="fas fa-cog"></i></a>
								</div>
							</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>System Architect</td>
							<td>Edinburgh</td>
							<td>61</td>
							<td>2011/04/25</td>
							<td>
								<div class="btn-group" role="group" aria-label="...">
									<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-info btn-xs"><i class="fas fa-eye"></i></button>
									<a href="company-manage.php" type="button" class="btn btn-warning btn-xs"><i class="fas fa-cog"></i></a>
								</div>
							</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>System Architect</td>
							<td>Edinburgh</td>
							<td>61</td>
							<td>2011/04/25</td>
							<td>
								<div class="btn-group" role="group" aria-label="...">
									<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-info btn-xs"><i class="fas fa-eye"></i></button>
									<a href="company-manage.php" type="button" class="btn btn-warning btn-xs"><i class="fas fa-cog"></i></a>
								</div>
							</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>System Architect</td>
							<td>Edinburgh</td>
							<td>61</td>
							<td>2011/04/25</td>
							<td>
								<div class="btn-group" role="group" aria-label="...">
									<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-info btn-xs"><i class="fas fa-eye"></i></button>
									<a href="company-manage.php" type="button" class="btn btn-warning btn-xs"><i class="fas fa-cog"></i></a>
								</div>
							</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>System Architect</td>
							<td>Edinburgh</td>
							<td>61</td>
							<td>2011/04/25</td>
							<td>
								<div class="btn-group" role="group" aria-label="...">
									<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-info btn-xs"><i class="fas fa-eye"></i></button>
									<a href="company-manage.php" type="button" class="btn btn-warning btn-xs"><i class="fas fa-cog"></i></a>
								</div>
							</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>System Architect</td>
							<td>Edinburgh</td>
							<td>61</td>
							<td>2011/04/25</td>
							<td>
								<div class="btn-group" role="group" aria-label="...">
									<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-info btn-xs"><i class="fas fa-eye"></i></button>
									<a href="company-manage.php" type="button" class="btn btn-warning btn-xs"><i class="fas fa-cog"></i></a>
								</div>
							</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>System Architect</td>
							<td>Edinburgh</td>
							<td>61</td>
							<td>2011/04/25</td>
							<td>
								<div class="btn-group" role="group" aria-label="...">
									<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-info btn-xs"><i class="fas fa-eye"></i></button>
									<a href="company-manage.php" type="button" class="btn btn-warning btn-xs"><i class="fas fa-cog"></i></a>
								</div>
							</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>System Architect</td>
							<td>Edinburgh</td>
							<td>61</td>
							<td>2011/04/25</td>
							<td>
								<div class="btn-group" role="group" aria-label="...">
									<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-info btn-xs"><i class="fas fa-eye"></i></button>
									<a href="company-manage.php" type="button" class="btn btn-warning btn-xs"><i class="fas fa-cog"></i></a>
								</div>
							</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>System Architect</td>
							<td>Edinburgh</td>
							<td>61</td>
							<td>2011/04/25</td>
							<td>
								<div class="btn-group" role="group" aria-label="...">
									<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-info btn-xs"><i class="fas fa-eye"></i></button>
									<a href="company-manage.php" type="button" class="btn btn-warning btn-xs"><i class="fas fa-cog"></i></a>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</main>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn modal-close" data-dismiss="modal">Close</button>
        <button type="button" class="btn modal-success">Save changes</button>
      </div>
    </div>
  </div>
</div>
<?php include('includes/footer.php') ?>
<script>
	$(document).ready(function() {
		$('.select2').select2({
			placeholder: 'Select',
		})
		$('#select2-multiple').select2({
			placeholder: 'Select',
		})
		

	    $('#example').DataTable({
            dom: 
            "<'row datatble-header-row'<'col col-sm-4 col-xs-12'l><'col col-sm-4 col-xs-12 mb-sm-5 text-center'><'col col-sm-4 col-xs-12'f>>" +
            "<'row datatble-body-row'<'col col-sm-12 col-xs-12'tr>>" +
            "<'row datatble-footer-row'<'col col-sm-5 col-xs-12'i><'col col-sm-7 col-xs-12'p>>",
	    });

	} );
</script>