<!DOCTYPE html>
<html>
	<head>
		<title>Tadbeer Admin</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<!-- Optional theme -->
		<link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="assets/css/theme-style.min.css">
		<link rel="stylesheet" href="assets/css/style.min.css">
		<link rel="stylesheet" href="assets/css/responsive.min.css">
		<!-- Font Awesome 5 -->
		<link href="assets/plugins/fontawesome-5.8.1/css/all.css" rel="stylesheet">
		
		<!-- DataTables -->
		<link rel="stylesheet" href="assets/plugins/DataTables/datatables.min.css">
		<!-- Open+Sans font-->
		<link href="assets/fonts/custom-font.css" rel="stylesheet">
		<!-- Select 2 -->
		<link href="assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet">
		
	</head>
	<body class="preload">
		<div class="container col-center flex-center">
			<div class="authentication-form">
				<form method="post" action="index.php">
					<div class="authentication-header">
						<img src="assets/img/icons/housekeepingco-logo-white.png" alt="housekeepingco-logo" class="img-responsive">
					</div>
					<div class="authentication-body">
						<h3>Login</h3>
						<div class="form-group">
							<label for="">Username</label>
							<input type="email" class="form-control" placeholder="">
						</div>
						<div class="form-group">
							<label for="">Password</label>
							<input type="Password" class="form-control" placeholder="">
						</div>
						<div class="form-group mbt-20">
							<label class="check-box">Remember me
								<input type="checkbox">
								<span class="checkmark"></span>
							</label>
						</div>
						<br>
						<div class="form-group">
							<button class="btn theme-button btn-block btn-lg">Login</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<script src="assets/plugins/jquery-3.3.1/jquery.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="assets/js/bootstrap.min.js"></script>
		<script>
			$(document).ready(function() {
				$("body").removeClass("preload");
			});

		</script>
	</body>
</html>