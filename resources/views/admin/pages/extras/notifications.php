<?php include('includes/header.php') ?>
<!-- Dashbaord Page -->
<main class="page-content">
	<div class="content-header">
		<div class="page-detail">
			<h2 class="page-heading">Notifications</h2>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fas fa-home"></i></a></li>
				<li class="active">Notifications</li>
			</ol>
		</div>
	</div>
	<div class="page-inner">
		<div class="card">
			<div class="card-header">
				<h5>All Notification Type</h5>
				<span class="text-muted"><b>Plugin: </b>Sweet Alert 2</span>
			</div>
			<div class="card-block">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<div class="btn-group" role="group" aria-label="...">
									<button onclick="basicMessage()" class="btn btn-default"><i class="fas fa-info-circle"></i> Basic Message</button>
									<button onclick="successMessage()" class="btn btn-success"><i class="fas fa-info-circle"></i> Success Message</button>
									<button onclick="warningMessage()" class="btn btn-warning"><i class="fas fa-info-circle"></i> Warning Message</button>
									<button onclick="confirmDialogue()" class="btn btn-danger"><i class="fas fa-info-circle"></i> Confirm Dialogue</button>
									<button onclick="dangerMessage()" class="btn btn-info"><i class="fas fa-info-circle"></i> Danger Message</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include('includes/footer.php') ?>
<link rel="stylesheet" href="assets/plugins/sweetalert2-master/sweetalert2.min.css">
<script src="assets/plugins/sweetalert2-master/sweetalert2.all.min.js"></script>

<script>
	function basicMessage(){
		Swal.fire('Dummy Text Generated!!')
	}

	function successMessage(){
		Swal.fire(
		  'Good job!',
		  'You clicked the button!',
		  'success'
		)
	}

	function warningMessage(){
		Swal.fire({
		  title: 'Warning Alert?',
		  text: "You won't be able to revert this!",
		  type: 'warning',
		})
	}

	function dangerMessage(){
		Swal.fire({
		  type: 'error',
		  title: 'Oops...',
		  text: 'Something went wrong!',
		})
	}

	function confirmDialogue(){
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
		  if (result.value) {
		    Swal.fire(
		      'Deleted!',
		      'Your file has been deleted.',
		      'success'
		    )
		  }
		})
	}
</script>