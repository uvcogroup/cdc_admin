<?php include('includes/header.php') ?>
<!-- Dashbaord Page -->
<main class="page-content">
	<div class="content-header">
		<div class="page-detail">
			<h2 class="page-heading">Company manage</h2>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fas fa-home"></i></a></li>
				<li><a href="company-setup.php">Company Setup</a></li>
				<li class="active">Company manage</li>
			</ol>
		</div>
	</div>
	<div class="page-inner">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-md-4">
						<div class="panel theme-panel panel-heading-300">
		                    <div class="panel-heading">
		                        <a href="#Onecollapse" data-toggle="collapse">
		                            <h3 class="panel-title"><b>Information Heading</b></h3>
		                        </a>
		                    </div>
		                    <div class="clearfix"></div>
		                    <div id="Onecollapse" data-toggle="collapse" class="panel-collapse collapse in">
		                        <div class="panel-body">
		                            <table class="table manage-detail-table table-bordered">
		                                <tbody>
		                                    <tr>
		                                        <th width="35%">Heading</th>
		                                        <td>Detail</td>
		                                    </tr>
		                                    <tr>
		                                        <th width="35%">Heading</th>
		                                        <td>Detail</td>
		                                    </tr>
		                                    <tr>
		                                        <th width="35%">Heading</th>
		                                        <td>Detail</td>
		                                    </tr>
		                                </tbody>
		                            </table>
		                        </div>
		                    </div>
		                </div>
					</div>
					<div class="col-md-4">
						<div class="panel theme-panel panel-heading-300">
		                    <div class="panel-heading">
		                        <a href="#twoCol" data-toggle="collapse">
		                            <h3 class="panel-title"><b>Information Heading</b></h3>
		                        </a>
		                    </div>
		                    <div class="clearfix"></div>
		                    <div id="twoCol" data-toggle="collapse" class="panel-collapse collapse in">
		                        <div class="panel-body">
		                            <table class="table manage-detail-table table-bordered">
		                                <tbody>
		                                    <tr>
		                                        <th width="35%">Heading</th>
		                                        <td>Detail</td>
		                                    </tr>
		                                    <tr>
		                                        <th width="35%">Heading</th>
		                                        <td>Detail</td>
		                                    </tr>
		                                    <tr>
		                                        <th width="35%">Heading</th>
		                                        <td class="readmore-box">
		                                        	<div class="readmore">
		                                        		Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo
		                                        	</div>
		                                        </td>
		                                    </tr>
		                                </tbody>
		                            </table>
		                        </div>
		                    </div>
		                </div>
					</div>
					<div class="col-md-4">
						<div class="panel theme-panel panel-heading-300">
		                    <div class="panel-heading">
		                        <a href="#threeCol" data-toggle="collapse">
		                            <h3 class="panel-title"><b>Information Heading</b></h3>
		                        </a>
		                    </div>
		                    <div class="clearfix"></div>
		                    <div id="threeCol" data-toggle="collapse" class="panel-collapse collapse in">
		                        <div class="panel-body">
		                            <table class="table manage-detail-table table-bordered">
		                                <tbody>
		                                    <tr>
		                                        <th width="35%">Heading</th>
		                                        <td>Detail</td>
		                                    </tr>
		                                    <tr>
		                                        <th width="35%">Heading</th>
		                                        <td>Detail</td>
		                                    </tr>
		                                    <tr>
		                                        <th width="35%">Heading</th>
		                                        <td>Detail</td>
		                                    </tr>
		                                </tbody>
		                            </table>
		                        </div>
		                    </div>
		                </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include('includes/footer.php') ?>
<script>
	$('.readmore').readmore({
		speed: 100,
		collapsedHeight: 20,
  		moreLink: '<a href="#">&raquo;</a>',
  		lessLink: '<a href="#">&laquo;</a>'
	});
</script>