<?php include('includes/header.php') ?>
<!-- Dashbaord Page -->
<main class="page-content">
	<div class="content-header">
		<div class="page-detail col-md-8">
			<h2 class="page-heading">Add Company Detail</h2>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fas fa-home"></i></a></li>
				<li><a href="company-setup.php">Company Setup</a></li>
				<li class="active">Add Company Detail</li>
			</ol>
		</div>
	</div>
	<form action="">
		<div class="page-inner">
			<div class="card">
				<div class="card-block">
					<div class="row">
						<div class="col-md-12">
							<div class="panel theme-panel panel-heading-600 panel-form">
			                    <div class="panel-heading">
			                        <a href="#Onecollapse" data-toggle="collapse">
			                            <h3 class="panel-title"><b>Information Heading</b></h3>
			                        </a>
			                    </div>
			                    <div class="clearfix"></div>
			                    <div id="Onecollapse" data-toggle="collapse" class="panel-collapse collapse in">
			                        <div class="panel-body row-equal">
			                            <div class="col-md-6 col-sm-6 col-xs-12">
			                            	<div class="form-group display-lg-flex">
			                            		<div class="col-lg-4">
			                            			<label for="">Heading</label>
			                            		</div>
			                            		<div class="col-lg-8">
			                            			<input type="text" class="form-control" placeholder="Text Here">
			                            		</div>
			                            	</div>
			                            </div>
			                            <div class="col-md-6 col-sm-6 col-xs-12">
			                            	<div class="form-group display-lg-flex">
			                            		<div class="col-lg-4">
			                            			<label for="">Heading</label>
			                            		</div>
			                            		<div class="col-lg-8">
			                            			<input type="text" class="form-control" placeholder="Text Here">
			                            		</div>
			                            	</div>
			                            </div>
			                            <div class="col-md-6 col-sm-6 col-xs-12">
			                            	<div class="form-group display-lg-flex">
			                            		<div class="col-lg-4">
			                            			<label for="">Heading Select2</label>
			                            		</div>
			                            		<div class="col-lg-8">
													<select name="" class="form-control" id="select2">
														<option selected="true" disabled="disabled">Select</option>
														<option value="asdasd">One</option>
														<option value="asdasd">Two</option>
														<option value="asdasd">Three</option>
													</select>
			                            		</div>
			                            	</div>
			                            </div>
			                            <div class="col-md-6 col-sm-6 col-xs-12">
			                            	<div class="form-group display-lg-flex">
			                            		<div class="col-lg-4">
			                            			<label for="">Heading Select2</label>
			                            		</div>
			                            		<div class="col-lg-8">
													<select name="" class="form-control" id="select22">
														<option selected="true" disabled="disabled">Select</option>
														<option value="asdasd">One</option>
														<option value="asdasd">Two</option>
														<option value="asdasd">Three</option>
													</select>
			                            		</div>
			                            	</div>
			                            </div>
			                            <div class="col-md-6 col-sm-6 col-xs-12">
			                            	<div class="form-group display-lg-flex">
			                            		<div class="col-lg-4">
			                            			<label for="">Heading</label>
			                            		</div>
			                            		<div class="col-lg-8">
			                            			<input type="text" class="form-control" placeholder="Text Here">
			                            		</div>
			                            	</div>
			                            </div>
			                            <div class="col-md-6 col-sm-6 col-xs-12">
			                            	<div class="form-group display-lg-flex">
			                            		<div class="col-lg-4">
			                            			<label for="">Heading</label>
			                            		</div>
			                            		<div class="col-lg-8">
			                            			<input type="text" class="form-control" placeholder="Text Here">
			                            		</div>
			                            	</div>
			                            </div>
			                            <div class="col-md-6 col-sm-6 col-xs-12">
			                            	<div class="form-group display-lg-flex">
			                            		<div class="col-lg-4">
			                            			<label for="">Heading</label>
			                            		</div>
			                            		<div class="col-lg-8">
			                            			<input type="text" class="form-control" placeholder="Text Here">
			                            		</div>
			                            	</div>
			                            </div>
			                            <div class="col-md-6 col-sm-6 col-xs-12">
			                            	<div class="form-group display-lg-flex">
			                            		<div class="col-lg-4">
			                            			<label for="">Heading</label>
			                            		</div>
			                            		<div class="col-lg-8">
			                            			<input type="text" class="form-control" placeholder="Text Here">
			                            		</div>
			                            	</div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
						</div>
						<div class="col-md-12">
							<div class="panel theme-panel panel-heading-600 panel-form mb-0">
			                    <div class="panel-heading">
			                        <a href="#twoCollapse" data-toggle="collapse">
			                            <h3 class="panel-title"><b>Information Heading</b></h3>
			                        </a>
			                    </div>
			                    <div class="clearfix"></div>
			                    <div id="twoCollapse" data-toggle="collapse" class="panel-collapse collapse in">
			                        <div class="panel-body row-equal">
			                            <div class="col-md-6 col-sm-6 col-xs-12">
			                            	<div class="form-group display-lg-flex">
			                            		<div class="col-lg-4">
			                            			<label for="">Heading</label>
			                            		</div>
			                            		<div class="col-lg-8">
			                            			<input type="text" class="form-control" placeholder="Text Here">
			                            		</div>
			                            	</div>
			                            </div>
			                            <div class="col-md-6 col-sm-6 col-xs-12">
			                            	<div class="form-group display-lg-flex">
			                            		<div class="col-lg-4">
			                            			<label for="">Heading</label>
			                            		</div>
			                            		<div class="col-lg-8">
			                            			<input type="text" class="form-control" placeholder="Text Here">
			                            		</div>
			                            	</div>
			                            </div>
			                            <div class="col-md-6 col-sm-6 col-xs-12">
			                            	<div class="form-group display-lg-flex">
			                            		<div class="col-lg-4">
			                            			<label for="">Heading</label>
			                            		</div>
			                            		<div class="col-lg-8">
			                            			<input type="text" class="form-control" placeholder="Text Here">
			                            		</div>
			                            	</div>
			                            </div>
			                            <div class="col-md-6 col-sm-6 col-xs-12">
			                            	<div class="form-group display-lg-flex">
			                            		<div class="col-lg-4">
			                            			<label for="">Heading</label>
			                            		</div>
			                            		<div class="col-lg-8">
			                            			<input type="text" class="form-control" placeholder="Text Here">
			                            		</div>
			                            	</div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
						</div>
					</div>
				</div>
				<div class="card-footer-button text-center">
					<button class="btn btn-cancel"><i class="fas fa-times"></i> Cancel</button>
					<button class="btn btn-submit"><i class="fas fa-save"></i> Save</button>
				</div>
			</div>
		</div>
	</form>
</main>
<?php include('includes/footer.php') ?>
<script>
	$('#select2, #select22').select2({
		placeholder: "Select",
	});
</script>