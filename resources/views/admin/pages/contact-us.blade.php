@extends('layouts.main')
@section('title', "Contact Us")
@section('content')
<!-- Dashbaord Page -->
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-confirm-master/css/jquery-confirm.css') }}">
<main class="page-content">
	<div class="content-header">
		<div class="page-detail col-md-8">
			<h2 class="page-heading">Update Contact Us</h2>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fas fa-home"></i></a></li>
				<li class="active">Update Contact Us</li>
			</ol>
		</div>
	</div>

	<form action="{{ route('main.contact-us-post') }}" method="post">
        @csrf
            <div class="page-inner">
                <div class="card">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel theme-panel panel-heading-600 panel-form mb-0">
                                    <div class="panel-heading">
                                        <a href="#Onecollapse" data-toggle="collapse">
                                            <h3 class="panel-title"><b>Contact Us Detail</b></h3>
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div id="Onecollapse" data-toggle="collapse" class="panel-collapse collapse in">
                                        <div class="panel-body row-equal">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group display-lg-flex">
                                                    <div class="col-lg-4">
                                                        <label for="">Address</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        @foreach (json_decode($data->address) as $key => $item)
                                                            <input type="text" class="form-control mb-5" value="{{ $item }}" name="address[]" placeholder="Address Line {{ $key+1 }}">
                                                        @endforeach
                                                        @if($errors->has('address[]'))
                                                            <label class="error">{{ $errors->first('address[]') }}</label>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group display-lg-flex">
                                                    <div class="col-lg-4">
                                                        <label for="">Phone</label>
                                                    </div>
                                                    <div class="col-lg-8 phone-wrapper">
                                                        @foreach (json_decode($data->phone_numbers) as $key => $item)
                                                            <div class="input-group mb-5">
                                                                <span class="input-group-addon">+254</span>
                                                                <input type="number" class="form-control" value="{{ $item }}" placeholder="Enter Number here" name="phone_number[]">
                                                                <div class="input-group-btn">
                                                                    @if($key < 1)
                                                                        <button class="btn btn-success addMorePhone"><i class="fa fa-plus"></i></button>
                                                                    @else
                                                                        <button class="btn btn-danger removePhone"><i class="fa fa-minus"></i></button>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                        @if($errors->has('phone_number[]'))
                                                            <label class="error">{{ $errors->first('phone_number[]') }}</label>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group display-lg-flex">
                                                    <div class="col-lg-4">
                                                        <label for="">Email</label>
                                                    </div>
                                                    <div class="col-lg-8 email-wrapper">
                                                        @foreach (json_decode($data->emails) as $key => $item)
                                                            <div class="input-group mb-5">
                                                                <input type="email" class="form-control" value="{{ $item }}" placeholder="Enter Email here" name="email[]">
                                                                <div class="input-group-btn">
                                                                    @if($key < 1)
                                                                        <button class="btn btn-success addMoreEmail"><i class="fa fa-plus"></i></button>
                                                                    @else
                                                                        <button class="btn btn-danger removeEmail"><i class="fa fa-minus"></i></button>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        @endforeach
                                                        @if($errors->has('email[]'))
                                                            <label class="error">{{ $errors->first('email[]') }}</label>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group display-lg-flex">
                                                    <div class="col-lg-4">
                                                        <label for="">Map URL</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <textarea name="map_url" rows="5" class="form-control" placeholder="Enter Map URL here">{{ $data->map_url }}</textarea>
                                                        @if($errors->has('map_url'))
                                                            <label class="error">{{ $errors->first('map_url') }}</label>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer-button text-center">
                        <button class="btn btn-cancel"><i class="fas fa-times"></i> Cancel</button>
                        <button class="btn btn-submit"><i class="fas fa-save"></i> Save</button>
                    </div>
                </div>
            </div>
        </form>
</main>
@endSection
@push('footer-js')
    <script src="{{ asset('assets/plugins/jquery-confirm-master/js/jquery-confirm.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2({
                placeholder: 'Select',
            })
            $('#select2-multiple').select2({
                placeholder: 'Select',
            })


            $('#example').DataTable({
                dom:
                "<'row datatble-header-row'<'col col-sm-4 col-xs-12'l><'col col-sm-4 col-xs-12 mb-sm-5 text-center'><'col col-sm-4 col-xs-12'f>>" +
                "<'row datatble-body-row'<'col col-sm-12 col-xs-12'tr>>" +
                "<'row datatble-footer-row'<'col col-sm-5 col-xs-12'i><'col col-sm-7 col-xs-12'p>>",
            });

        });

        // For Mobile
        $('.addMorePhone').on('click', function (e) {
            e.preventDefault();
            $this = $(this);
            count = $this.parents('.phone-wrapper').find('.input-group').length;
            $html = '<div class="input-group mb-5">'+
                        '<span class="input-group-addon">+254</span>'+
                        '<input type="number" class="form-control" placeholder="Enter Number here" name="phone_number[]">'+
                        '<div class="input-group-btn">'+
                            '<button class="btn btn-danger removePhone"><i class="fa fa-minus"></i></button>'+
                        '</div>'+
                    '</div>';
            if(count < 3){
                $this.parents('.phone-wrapper').append($html);
            }else{
                $.alert('Maximum 3 Mobile Numbers are allowed');
            }
        })
        $('body').on('click', '.removePhone', function (e) {
            e.preventDefault();
            $(this).parents('.input-group').remove();
        })

        // For Emails
        $('.addMoreEmail').on('click', function (e) {
            e.preventDefault();
            $this = $(this);
            $html = '<div class="input-group mb-5">'+
                        '<input type="email" class="form-control" placeholder="Enter Email here" name="email[]">'+
                        '<div class="input-group-btn">'+
                            '<button class="btn btn-danger removeEmail"><i class="fa fa-minus"></i></button>'+
                        '</div>'+
                    '</div>';
            count = $this.parents('.email-wrapper').find('.input-group').length;
            if(count < 3){
                $this.parents('.email-wrapper').append($html);
            }else{
                $.alert('Maximum 3 Emails are allowed');
            }
        })
        $('body').on('click', '.removeEmail', function (e) {
            e.preventDefault();
            $(this).parents('.input-group').remove();
        })

    </script>
@endpush
