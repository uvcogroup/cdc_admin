@extends('layouts.main')
@section('title', "Dashbaord")
@section('content')
<!-- Dashbaord Page -->
<main class="page-content">
	<div class="content-header">
		<div class="page-detail">
			<h2 class="page-heading">Dashboard</h2>
		</div>
	</div>
	<div class="content-inner">
		<div class="row">
			<div class="col-md-12">
				<div class="content-box box-primary matchHeight">
					<div class="box-head">
						<div class="col-md-12">
							<h4></h4>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="box-body">
						<div class="col-lg-4 widgetCustomWidth">
							<div class="dashoard-widget">
								<div class="widget-heading">
									<h5>Total Doctors</h5>
								</div>
								<div class="widget-detail">
									<h5 id="">24</h5>
								</div>
							</div>
						</div>
						<div class="col-lg-4 widgetCustomWidth">
							<div class="dashoard-widget">
								<div class="widget-heading">
									<h5>Total Lead From Contact Us</h5>
								</div>
								<div class="widget-detail">
									<h5 id="">20</h5>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="box-footer"></div>
				</div>
			</div>
		</div>
	</div>
</main>
@endsection
