@extends('layouts.main')
@section('title', "Doctors")
@section('content')
@php
    if(isset($data)){

        $title = old('title') ?? $data->title;
        $percentage = old('percentage') ?? $data->percentage;
        $description = old('description') ?? $data->description;
        $status = old('status') ?? $data->status;

    }else if(old()){

        $title = old("title");
        $percentage = old("percentage");
        $description = old("description");
        $status = old("status");

    }else{

        $title = null;
        $percentage = null;
        $description = null;
        $status = null;

    }
@endphp
<main class="page-content">
	<div class="content-header">
		<div class="page-detail col-md-8">
			<h2 class="page-heading">{{ $flag }} Offer</h2>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fas fa-home"></i></a></li>
				<li class="active">{{ $flag }} Offer</li>
			</ol>
		</div>
	</div>
	<div class="page-inner">
        <form action="{{ $route }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card">
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel theme-panel panel-heading-600 panel-form mb-0">
                                <div class="panel-heading">
                                    <a href="#Onecollapse" data-toggle="collapse">
                                        <h3 class="panel-title"><b>Make new offer for Comprehesive Diabeties center</b></h3>
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                                <div id="Onecollapse" class="panel-collapse collapse in">
                                    <div class="panel-body row-equal">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group display-lg-flex">
                                                <div class="col-lg-3">
                                                    <label for="">Offer Image</label>
                                                </div>
                                                <div class="col-lg-9">
                                                    <img id="blah" src="@if(isset($data)) {{ asset($data->bannerImage->c_img) }} @else {{ asset('assets/img/banner-default.png') }} @endif" class="mb-5" height="200" alt="">
                                                    <input @if(!isset($data)) required @endif id="imgInp" type="file" class="form-control" name="image" placeholder="Text Here">
                                                    @if($errors->has('image'))
                                                        <label class="error">{{ $errors->first('image') }}</label>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group display-lg-flex">
                                                <div class="col-lg-3">
                                                    <label for="">Title</label>
                                                </div>
                                                <div class="col-lg-9">
                                                    <input required type="text" value="{{ $title }}" name="title" class="form-control" placeholder="Enter Title here">
                                                    @if($errors->has('title'))
                                                        <label class="error">{{ $errors->first('title') }}</label>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group display-lg-flex">
                                                <div class="col-lg-3">
                                                    <label for="">Offer Percentage</label>
                                                </div>
                                                <div class="col-lg-9">
                                                    <input required type="text" value="{{ $percentage }}" name="percentage" class="form-control" placeholder="Enter Percentage here">
                                                    @if($errors->has('percentage'))
                                                        <label class="error">{{ $errors->first('percentage') }}</label>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group display-lg-flex">
                                                <div class="col-lg-3">
                                                    <label for="">Description</label>
                                                </div>
                                                <div class="col-lg-9">
                                                    <textarea required name="description" rows="3" class="form-control" placeholder="Enter Description here">{{ $description }}</textarea>
                                                    @if($errors->has('description'))
                                                        <label class="error">{{ $errors->first('description') }}</label>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group display-lg-flex">
                                                <div class="col-lg-3">
                                                    <label for="">Status</label>
                                                </div>
                                                <div class="col-lg-9">
                                                    <select required name="status" class="form-control select2" id="status">
                                                        <option value=""></option>
                                                        <option value="Available">Available</option>
                                                        <option value="Unavailable">Unavailable</option>
                                                    </select>
                                                    <label id="status-error" class="error" for="status"></label>
                                                    @if($errors->has('status'))
                                                        <label class="error">{{ $errors->first('status') }}</label>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer-button text-center">
                    <button class="btn btn-cancel"><i class="fas fa-times"></i> Cancel</button>
                    <button type="submit" class="btn btn-submit"><i class="fas fa-envelope"></i> Send Email</button>
                </div>
            </div>
        </form>
	</div>
</main>
@endSection
@push('footer-js')
<script src="{{ asset('assets/plugins/jquery-validation-master/jquery.validate.min.js') }}"></script>
<script>
    $.fn.SidebarActive("{{ route('main.offer.index') }}");
    $('select[name="status"]').val("{{ $status }}").trigger('change');
	$(document).ready(function() {
		$('.select2').select2({
			placeholder: 'Select',
		})
		$('#select2-multiple').select2({
			placeholder: 'Select',
		})
	} );
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
            $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function() {
        readURL(this);
    });

    $("form").validate({
        submitHandler: function (form){
            form.submit();
        }
    });
</script>

@endpush
