@extends('layouts.main')
@section('title', "$flag Doctors")
@section('content')

@php
    if(isset($data)){

        $name = old('name') ?? $data->name;
        $phone_number = old('phone_number') ?? $data->phone_number;
        $email = old('email') ?? $data->email;
        $status = old('status') ?? $data->status;

    }else if(old()){

        $name = old("name");
        $phone_number = old("phone_number");
        $email = old("email");
        $status = old("status");

    }else{

        $name = null;
        $phone_number = null;
        $email = null;
        $status = null;

    }
@endphp
<main class="page-content">
	<div class="content-header">
		<div class="page-detail col-md-8">
			<h2 class="page-heading">{{ $flag }} Doctor</h2>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fas fa-home"></i></a></li>
				<li><a href="{{ route('main.doctors') }}">Doctors</a></li>
				<li class="active">{{ $flag }} Doctor</li>
			</ol>
		</div>
	</div>
    <form action="{{ $route }}" method="post" enctype="multipart/form-data">
        @csrf
		<div class="page-inner">
			<div class="card">
				<div class="card-block">
					<div class="row">
						<div class="col-md-12">
							<div class="panel theme-panel panel-heading-600 panel-form mb-0">
			                    <div class="panel-heading">
			                        <a href="#Onecollapse" data-toggle="collapse">
			                            <h3 class="panel-title"><b>Doctor Information</b></h3>
			                        </a>
			                    </div>
			                    <div class="clearfix"></div>
			                    <div id="Onecollapse" class="panel-collapse collapse in">
			                        <div class="panel-body row-equal">
			                            <div class="col-md-6 col-sm-6 col-xs-12">
			                            	<div class="form-group display-lg-flex">
			                            		<div class="col-lg-4">
			                            			<label for="">Profile Image</label>
			                            		</div>
			                            		<div class="col-lg-8 text-center">
                                                    <img id="blah" src="@if(isset($data)) {{ asset($data->profileImage->c_img) }} @else {{ asset('assets/img/default-doctor.jpg') }} @endif" class="mb-5" height="100" alt="">
			                            			<input @if(!isset($data)) required @endif id="imgInp" type="file" class="form-control" name="image" placeholder="Text Here">
                                                    @if($errors->has('image'))
                                                        <label class="error">{{ $errors->first('image') }}</label>
                                                    @endif
			                            		</div>
			                            	</div>
                                        </div>
			                            <div class="col-md-12 col-sm-12 col-xs-12"></div>
			                            <div class="col-md-6 col-sm-6 col-xs-12">
			                            	<div class="form-group display-lg-flex">
			                            		<div class="col-lg-4">
			                            			<label for="">Name</label>
			                            		</div>
			                            		<div class="col-lg-8">
			                            			<input required type="text" value="{{ $name }}" class="form-control" name="name" placeholder="Enter Name here">
                                                    @if($errors->has('name'))
                                                        <label class="error">{{ $errors->first('name') }}</label>
                                                    @endif
			                            		</div>
			                            	</div>
			                            </div>
			                            <div class="col-md-6 col-sm-6 col-xs-12">
			                            	<div class="form-group display-lg-flex">
			                            		<div class="col-lg-4">
			                            			<label for="">Phone Number</label>
			                            		</div>
			                            		<div class="col-lg-8">
                                                    <div class="input-group mb-0">
                                                        <span class="input-group-addon">+254</span>
                                                        <input required type="number" value="{{ $phone_number }}" class="form-control" value="" placeholder="Enter Number here" name="phone_number">
                                                    </div>
                                                    @if($errors->has('phone_number'))
                                                        <label class="error">{{ $errors->first('phone_number') }}</label>
                                                    @endif
			                            		</div>
			                            	</div>
			                            </div>
			                            <div class="col-md-6 col-sm-6 col-xs-12">
			                            	<div class="form-group display-lg-flex">
			                            		<div class="col-lg-4">
			                            			<label for="">Email</label>
			                            		</div>
			                            		<div class="col-lg-8">
			                            			<input required type="email" value="{{ $email }}" class="form-control" name="email" placeholder="Enter Email here">
                                                    @if($errors->has('email'))
                                                        <label class="error">{{ $errors->first('email') }}</label>
                                                    @endif
			                            		</div>
			                            	</div>
			                            </div>
			                            <div class="col-md-6 col-sm-6 col-xs-12">
			                            	<div class="form-group display-lg-flex">
			                            		<div class="col-lg-4">
			                            			<label for="">Status</label>
			                            		</div>
			                            		<div class="col-lg-8">
			                            			<select required name="status" class="form-control select2" id="status">
                                                        <option value=""></option>
                                                        <option value="Available">Available</option>
                                                        <option value="Unavailable">Unavailable</option>
                                                    </select>
                                                    @if($errors->has('status'))
                                                        <label class="error">{{ $errors->first('status') }}</label>
                                                    @endif
			                            		</div>
			                            	</div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
						</div>
					</div>
				</div>
				<div class="card-footer-button text-center">
					<button class="btn btn-cancel"><i class="fas fa-times"></i> Cancel</button>
					<button class="btn btn-submit"><i class="fas fa-save"></i> Save</button>
				</div>
			</div>
		</div>
	</form>
</main>
@endSection
@push('footer-js')
<script>
    $.fn.SidebarActive("{{ route('main.doctors') }}");
    $('select[name="status"]').val("{{ $status }}").trigger('change');
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
    }

    $("#imgInp").change(function() {
        readURL(this);
    });

    $("form").validate({
        submitHandler: function (form){
            form.submit();
        }
    });
</script>
@endpush

