@extends('layouts.main')
@section('title', "Doctors")
@section('content')
<main class="page-content">
	<div class="content-header">
		<div class="page-detail col-md-8">
			<h2 class="page-heading">Doctors</h2>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fas fa-home"></i></a></li>
				<li class="active">Doctors</li>
			</ol>
		</div>
		<div class="page-detail col-md-4 flex-right text-right">
			<a href="{{ route('main.doctors-post') }}" class="btn btn-theme first-header-button"> <i class="fas fa-plus"></i> Add Doctor</a>
		</div>
	</div>
	<div class="page-inner">
		<div class="card">
			<div class="card-block">
				<table id="example" class="display table-condensed action-table" style="width:100%">
					<thead>
						<tr>
							<th>ID</th>
							<th>Image</th>
							<th>Name</th>
							<th>Phone Number</th>
							<th>Email</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</main>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn modal-close" data-dismiss="modal">Close</button>
        <button type="button" class="btn modal-success">Save changes</button>
      </div>
    </div>
  </div>
</div>
@endSection
@push('footer-js')
<script>
	$(document).ready(function() {
		$('.select2').select2({
			placeholder: 'Select',
		})
		$('#select2-multiple').select2({
			placeholder: 'Select',
		})


	    $('#example').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{{ url()->current() }}",
            dom:
            "<'row datatble-header-row'<'col col-sm-4 col-xs-12'l><'col col-sm-4 col-xs-12 mb-sm-5 text-center'><'col col-sm-4 col-xs-12'f>>" +
            "<'row datatble-body-row'<'col col-sm-12 col-xs-12'tr>>" +
            "<'row datatble-footer-row'<'col col-sm-5 col-xs-12'i><'col col-sm-7 col-xs-12'p>>",
            columns: [
                { data: 'id', name: 'id', "sClass" : "text-center" },
                { data: 'image', name: 'id', "sClass" : "text-center"},
                { data: 'name', name: 'name'},
                { data: 'phone_number', name: 'phone_number'},
                { data: 'email', name: 'email'},
                { data: 'status', name: 'status', "sClass" : "text-center"},
                { data: 'action', name: 'id', "sClass" : "text-center"},
            ],
	    });

	} );
</script>

@endpush
