<!DOCTYPE html>
<html>
	<head>
		<title>Comprehensive Diabetes Center | Login</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
		<!-- Optional theme -->
		<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-theme.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/theme-style.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/style.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/responsive.min.css') }}">
		<!-- Font Awesome 5 -->
		<link href="{{ asset('assets/plugins/fontawesome-5.8.1/css/all.css') }}" rel="stylesheet">
		<!-- DataTables -->
		<link rel="stylesheet" href="{{ asset('assets/plugins/DataTables/datatables.min.css') }}">
		<!-- Open+Sans font-->
		<link href="{{ asset('assets/fonts/custom-font.css') }}" rel="stylesheet">
		<!-- Select 2 -->
		<link href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet">

	</head>
	<body class="preload">
		<div class="container col-center flex-center">
			<div class="authentication-form">
                <form method="POST" action="{{ route('main.login') }}">
                    @csrf
					<div class="authentication-header">
						<h1>Admin Panel</h1>
					</div>
					<div class="authentication-body">
						<h3>Login</h3>
						<div class="form-group">
							<label for="">Email</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                                <label class="invalid-feedback error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
						</div>
						<div class="form-group">
							<label for="">Password</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                                <label class="invalid-feedback error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
						</div>
						<div class="form-group mbt-20">
							<label class="check-box">Remember me
								<input type="checkbox">
								<span class="checkmark"></span>
							</label>
						</div>
						<br>
						<div class="form-group">
							<button type="submit" class="btn theme-button btn-block btn-lg">Login</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<script src="{{ asset('assets/plugins/jquery-3.3.1/jquery.min.js') }}"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
		<script>
			$(document).ready(function() {
				$("body").removeClass("preload");
			});
		</script>
	</body>
</html>
