
// Single Date Picker
minDate = "";
getMinDate = $(".singleDatePicker").attr("min-date");
if(getMinDate == "0"){
    options = {
        "singleDatePicker": true,
        "locale": {
            "format": "DD-MM-YYYY",
        },
    };
}else{
    options = {
        "singleDatePicker": true,
        "minDate": moment(new Date()),
        "locale": {
            "format": "DD-MM-YYYY",
        },
    };
}

$(".singleDatePicker").daterangepicker(options);

$(".sidebar-dropdown > a").click(function() {
    $(".sidebar-submenu").slideUp(200);
    if (
    $(this)
        .parent()
        .hasClass("active")
    ) {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
        .parent()
        .removeClass("active");
    } else {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
        .next(".sidebar-submenu")
        .slideDown(200);
    $(this)
        .parent()
        .addClass("active");
    }
});

$("#close-sidebar").click(function() {
    $(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function() {
    $(".page-wrapper").addClass("toggled");
});

$.fn.getPlaceholder = function(){
    $("form input[type='text'], form input[type='email'], form input[type='number'], form input[type='password'], form textarea").each(function(){
        var input = $(this);
        var CheckPlaceholder = $(this).attr('data-placeholder');
        var DontHavePlaceholder = $(this).not('data-placeholder');
        if(CheckPlaceholder == "false"){
            $(input).attr('placeholder')
        }
        else if(DontHavePlaceholder){
            LabelNames = input.closest('.form-group').find('label:not(".error")').text();
            $(input).attr('placeholder', 'Enter '+LabelNames+' Here')
        }
    });
}

$('.browse-btn').on('click', function(){
    parentClass = $(this).parent('.input-container').find('input[type="file"]');
    fileNamePlace = $(this).parent('.input-container').find('.file-info');
    parentClass.click()
    $(parentClass).on('change', function(e){
        fileName = e.target.files[0].name;
        fileNamePlace.text(fileName)
    })
})
    
$('.select2').select2({
    placeholder: 'Select',
})

$("body").on("click", "button", function(e){
    href = $(this).attr("href");
    if(href){
        window.location.href = href;
    }
})